package com.likyh.app.blindapp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by linyh on 2014/10/17.
 */
public class SettingsActivity extends PreferenceActivity
        implements Preference.OnPreferenceChangeListener {
    private SharedPreferences mSharedPrefs;
    private CheckBoxPreference mList1Prefs;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        addPreferencesFromResource(R.xml.setting);
        mList1Prefs = (CheckBoxPreference) findPreference("voiceNav");
        mList1Prefs.setOnPreferenceChangeListener(this);
    }

    public boolean onPreferenceChange(Preference preference, Object newValue) {

        boolean prefsValue = mSharedPrefs.getBoolean(preference.getKey(), false);
        Log.v("pref",preference.toString()+(prefsValue?"true":"false"));
        return true;
    }
}
