package com.likyh.app.blindapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.baidu.speechsynthesizer.publicutility.SpeechError;
import com.likyh.app.blindapp.data.BaseData;
import com.likyh.app.blindapp.data.NewsMode;
import com.likyh.app.blindapp.data.TtsMode;
import com.likyh.app.blindapp.view.MyDialogManager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by linyh on 2014/10/21.
 */
public class NewsGuideActivity extends GuideActivity {
    private NewsMode newsMode;
    protected MyDialogManager myDialogManager;

    protected Handler dataHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(!messageListener(msg)){
                super.handleMessage(msg);
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        newsMode = NewsMode.init();
        myDialogManager=new MyDialogManager(this);
        ttsMode.speakMonopoly("加载中");
        myDialogManager.progressCreate("Loading","读取新闻数据中，请稍后").show();
        new Thread(new Runnable(){
            @Override
            public void run() {
                BaseData<NewsMode.NewsListData> a = newsMode.list(20,0,true);
                dataHandler.sendMessage(dataHandler.obtainMessage(0, a));
            }
        }).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    protected void userStart(){
        ttsMode.speak("欢迎进入新闻页面，上下滑动选择不同的新闻，长按进入当前新闻，左滑进入精品页面");
        setTipsText(newsMode.getCurrent().getTitle());
        ttsMode.speak("当前新闻，" + newsMode.getCurrent().getTitle());

        setConfirmText("进入");
        setLeftText("精品");
        setRightText("听书(建设中)");
        setUpText("上一篇新闻");
        setDownText("下一篇新闻");
        setClockText("顺时针：设置(慎用，无语音导航)");
        setAntiClockText(null);
    }

    protected void newsChangeWith(int a){
        newsMode.indexChangeCycle(a);
        setTipsText(newsMode.getCurrent().getTitle());
        ttsMode.speakMonopoly(newsMode.getCurrent().getTitle());
    }

    @Override
    public boolean onAntiClockEvent() {
        ttsMode.speakMonopoly("退出");
        return true;
    }

    @Override
    public boolean onLeftEvent() {
        Intent intent = new Intent(this, BoutiqueActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public boolean onRightEvent() {
        ttsMode.speakMonopoly("听书");
        return true;
    }

    @Override
    public boolean onDownEvent() {
        newsChangeWith(1);
        return true;
    }

    @Override
    public boolean onUpEvent() {
        newsChangeWith(-1);
        return true;
    }

    @Override
    public boolean onClockEvent() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
        return true;
    }

    @Override
    public boolean onConfirmEvent() {
        Intent intent = new Intent(this, NewsDetailGuideActivity.class);
        startActivity(intent);
        return true;
    }

    protected boolean messageListener(Message m){
        BaseData<NewsMode.NewsListData> data=(BaseData<NewsMode.NewsListData>)m.obj;

        ((ProgressDialog)myDialogManager.find("Loading")).dismiss();
        String tips=null;
        if(data!=null){
            if(data.getState()==200&&data.getData()!=null){
                userStart();
                return true;
            }else{
                tips = data.getMessage();
            }
        }else{
            tips="抱歉，网络请求错误";
        }
        ttsMode.speak(tips);
        return true;
    }
}
