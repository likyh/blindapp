package com.likyh.app.blindapp;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;

import com.likyh.app.blindapp.data.NewsMode;
import com.likyh.app.blindapp.data.TtsMode;

import java.io.IOException;

/**
 * Created by linyh on 2014/10/21.
 */
public class BoutiqueActivity extends GuideActivity {
    private boolean ifPlay=false;
    protected MediaPlayer mp;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mp = new MediaPlayer();
        ttsMode.speakMonopoly("欢迎进入精品页面，逆时针旋转退出当前界面，长按播放或者暂停", new TtsMode.TtsListener() {
            @Override
            public void onFinish(int type) {
                try {
                    mp.setDataSource("http://blindapp.qiniudn.com/donglina.mp3");
                    mp.prepare();
                    mp.start();
                    ifPlay=true;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        setConfirmText("播放/暂停");
        setLeftText(null);
        setRightText(null);
        setUpText("没有上一篇了");
        setDownText("没有下一篇了");
        setClockText(null);
        setAntiClockText(null);
        setTipsText(null);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mp.stop();
    }

    @Override
    public boolean onDownEvent() {
        ttsMode.speak("没有下一篇了");
        return true;
    }

    @Override
    public boolean onUpEvent() {
        ttsMode.speak("没有上一篇了");
        return true;
    }

    @Override
    public boolean onAntiClockEvent() {
        this.finish();
        return true;
    }

    @Override
    public boolean onConfirmEvent() {
        if(ifPlay){
            mp.pause();
            ifPlay=false;
        }else{
            mp.start();
            ifPlay=true;
        }
        return true;
    }
}
