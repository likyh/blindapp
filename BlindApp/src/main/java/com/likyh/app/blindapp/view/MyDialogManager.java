package com.likyh.app.blindapp.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by linyh on 2014/10/20.
 */
public class MyDialogManager {
    Context context;
    Map<String,Object> dislogMap=new HashMap<String,Object>();

    public MyDialogManager(Context context) {
        this.context = context;
    }

    public void dialogAdd(String title,Object o){
        dislogMap.put(title,o);
    }

    public ProgressDialog progressCreate(String title, String message){
        ProgressDialog d;
        d = new ProgressDialog(context);
        d.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        d.setTitle(title);//设置标题
        d.setMessage(message);
        d.setCancelable(false);
        d.setIndeterminate(false);//设置进度条是否为不明确
        dislogMap.put(title, d);
        return d;
    }

    public Object find(String title){
        return dislogMap.get(title);
    }
}
