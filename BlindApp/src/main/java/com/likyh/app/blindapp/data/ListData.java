package com.likyh.app.blindapp.data;

import java.util.List;

/**
 * Created by linyh on 2014/10/16.
 */
public interface ListData {
    public int getTotal();

    public void setTotal(int total);

    public int getCount();

    public void setCount(int count);

    public List getList();

    public void setList(List list) ;
}