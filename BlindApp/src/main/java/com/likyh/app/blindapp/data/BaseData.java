package com.likyh.app.blindapp.data;

/**
 * Created by linyh on 2014/10/17.
 */
public class BaseData<T> {
    private T data;
    private String message;
    private int state;
    private int serverTime;
    private String serverDate;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getServerTime() {
        return serverTime;
    }

    public void setServerTime(int serverTime) {
        this.serverTime = serverTime;
    }

    public String getServerDate() {
        return serverDate;
    }

    public void setServerDate(String serverDate) {
        this.serverDate = serverDate;
    }

    @Override
    public String toString() {
        return "BaseMode{" +
                "data=" + data +
                ", message='" + message + '\'' +
                ", state=" + state +
                ", serverTime=" + serverTime +
                ", serverDate='" + serverDate + '\'' +
                '}';
    }
}
