package com.likyh.app.blindapp.util;

import android.content.Context;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.likyh.app.blindapp.R;

import java.util.ArrayList;

/**
 * Created by linyh on 2014/10/22.
 */
public class GuideScanner implements GestureOverlayView.OnGesturePerformedListener,GestureDetector.OnGestureListener,View.OnTouchListener {
    GestureLibrary mGestureLib;
    GestureDetector mGesture = null;
    GuideListener listener;
    public GuideScanner(Context context) {
        mGestureLib = GestureLibraries.fromRawResource(context, R.raw.gestures_4);
        mGestureLib.load();
        mGesture = new GestureDetector(context,this);
    }

    public GuideListener getListener() {
        return listener;
    }

    public void setListener(GuideListener listener) {
        Log.v("guideScanner","listener:"+listener.toString());
        this.listener = listener;
    }

    public boolean onTouch(View v, MotionEvent event) {
        return mGesture.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if(listener!=null) listener.onUnknownEvent();
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        if(listener!=null) listener.onConfirmEvent();
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if(listener!=null){
            velocityX=Math.abs(velocityX);
            velocityY=Math.abs(velocityY);

            float distanceX=e2.getX()-e1.getX();
            float distanceY=e2.getY()-e1.getY();
            if(velocityX>velocityY&& Math.abs(distanceX)>Math.abs(3*distanceY)){
                if(distanceX>0){
                    Log.v("guideOverlay", "left ntk");
                    return listener.onLeftEvent();
                }else{
                    Log.v("guideOverlay", "right ntk");
                    return listener.onRightEvent();
                }
            }else if(velocityY>velocityX&& Math.abs(distanceY)>Math.abs(3*distanceX)){
                if(distanceY>0){
                    Log.v("guideOverlay", "down ntk");
                    return listener.onDownEvent();
                }else{
                    Log.v("guideOverlay", "up ntk");
                    return listener.onUpEvent();
                }
            }else{
                return listener.onUnknownEvent();
            }
        }
        return false;
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        // 从手势库中查询匹配的内容，匹配的结果可能包括多个相似的结果，匹配度高的结果放在最前面
        ArrayList<Prediction> predictions = mGestureLib
                .recognize(gesture);
        if (predictions.size() > 0) {
            Prediction prediction = predictions.get(0);
            // 匹配的手势
            if (prediction.score > 1.0&& listener!=null) { // 越匹配score的值越大，最大为10
                Log.v("guideOverlay", prediction.name +" "+ prediction.score);
                if(prediction.name.equals("left")){
                    listener.onLeftEvent();
                }else if(prediction.name.equals("right")){
                    listener.onRightEvent();
                }else if(prediction.name.equals("clock")){
                    listener.onClockEvent();
                }else if(prediction.name.equals("anticlock")){
                    listener.onAntiClockEvent();
                }
            }
        }
    }

    public interface GuideListener {
        public boolean onLeftEvent();
        public boolean onRightEvent();
        public boolean onUpEvent();
        public boolean onDownEvent();
        public boolean onClockEvent();
        public boolean onAntiClockEvent();
        public boolean onConfirmEvent();
        public boolean onUnknownEvent();
    }

}
