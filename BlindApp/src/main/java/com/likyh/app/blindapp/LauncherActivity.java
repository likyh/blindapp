package com.likyh.app.blindapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by linyh on 14-10-9.
 */
public class LauncherActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGHT = 5000;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launcher_layout);

        SoundPool soundPool = new SoundPool(3, AudioManager.STREAM_MUSIC, 0);
        final HashMap<Integer, Integer> soundPoolMap = new HashMap<Integer, Integer>();
        soundPoolMap.put(1, soundPool.load(this, R.raw.laucher, 1));
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                soundPool.play(soundPoolMap.get(1), 1, 1, 0, 0, 1);
            }
        });

        update();
        final Class goWhere;
        if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("firstUse", false)){
            goWhere=WelcomeActivity.class;
        }else{
            goWhere=NewsGuideActivity.class;
        }

        new Handler().postDelayed(new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(LauncherActivity.this,goWhere);
                LauncherActivity.this.startActivity(mainIntent);
                LauncherActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);

    }

    public void update(){
        boolean ifUpdate=false;
        String version = "";
        try {
            // 检验是否需要更新
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            String preferenceVersion = sharedPreferences.getString("preferenceVersion", "");
            Log.v("package", "配置文件版本号:" + preferenceVersion);

            PackageManager packageManager = getPackageManager();
            PackageInfo packInfo = packageManager.getPackageInfo(getPackageName(),0);
            version = packInfo.versionName;
            Log.v("package", "当前版本号:"+version);

//            //TODO 调试语句
//            Log.v("package", "配置文件1:"+sharedPreferences.getAll().toString());
//            sharedPreferences.edit().clear().commit();
//            Log.v("package", "配置文件2:"+sharedPreferences.getAll().toString());

            ifUpdate=!preferenceVersion.equals(version);
        } catch (PackageManager.NameNotFoundException e) {
            Log.v("package", "获取版本失败");
            e.printStackTrace();
        }
        // 初始化，或者将旧版本更新
        if(ifUpdate){
            Log.v("package", "开始更新配置文件");
            updatePreference(version);
        }
    }

    protected void updatePreference(String versionName){
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sp.edit();
        // 更新版本号
        editor.putString("preferenceVersion", versionName);

        // 更新是否为第一次使用
        editor.putBoolean("firstUse", true);

        // 更新是否采用语音导航
        editor.putBoolean("voiceNav", sp.getBoolean("voiceNav", true));
        editor.commit();

        Log.v("package", "配置文件更新完成:" + sp.getAll().toString());
    }
}