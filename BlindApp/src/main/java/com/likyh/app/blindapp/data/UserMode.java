package com.likyh.app.blindapp.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.util.Hashtable;
import java.util.Map;

/**
 * Created by linyh on 2014/10/16.
 */
public class UserMode extends ApiBase {
    private final SharedPreferences sharedPreferences;
    private static UserMode selfObject;
    protected String loginApi="/api/user/login";

    public UserMode(Context context){
        this.sharedPreferences =context.getSharedPreferences("user", Context.MODE_PRIVATE);
    }
    public static UserMode init(Context context){
        if(selfObject==null){
            selfObject=new UserMode(context);
        }
        return selfObject;
    }
    public BaseData<UserData> login(String loginName,String password){
        Map<String, String> params=new Hashtable<String, String>();
        params.put("user",loginName);
        params.put("pass",this.makeMD5(this.makeMD5(password)));
        String jsonString=this.getData(loginApi, params);

        Gson gson = new Gson();
        BaseData<UserData> data = null;
        try{
            data = gson.fromJson(jsonString, new TypeToken<BaseData<UserData>>() {}.getType());
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return data;
    }

    public class UserData {
        protected int id;
        protected String user;
        protected String tel;
        protected String email;
        protected String level;
        protected String token;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUser() {
            return user;
        }

        public void setUser(String user) {
            this.user = user;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "UserMode{" +
                    "id=" + id +
                    ", user='" + user + '\'' +
                    ", tel='" + tel + '\'' +
                    ", email='" + email + '\'' +
                    ", level='" + level + '\'' +
                    ", token='" + token + '\'' +
                    '}';
        }
    }

}
