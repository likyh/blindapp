package com.likyh.app.blindapp.data;

import android.content.Context;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.baidu.speechsynthesizer.SpeechSynthesizer;
import com.baidu.speechsynthesizer.SpeechSynthesizerListener;
import com.baidu.speechsynthesizer.publicutility.SpeechError;
import com.baidu.speechsynthesizer.publicutility.SpeechLogger;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by linyh on 2014/10/22.
 */
public class TtsMode implements SpeechSynthesizerListener {
    public static final int TYPE_SUCCESS=0;
    public static final int TYPE_CANCEL=1;
    public static final int TYPE_ERROR =2;

    public SpeechSynthesizer speechSynthesizer;
    private static TtsMode selfObject;
    private Queue<TtsData> ttsDataQueue = new LinkedList<TtsData>();
    private TtsData ttsDataCurrent;
    private boolean ifBusy=false;
    protected Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            SpeechError e=(SpeechError) msg.obj;
            onError(speechSynthesizer, e);
        }
    };

    private TtsMode() {}
    public static TtsMode init(Context context){
        if(selfObject==null) {
            selfObject=new TtsMode();
            SpeechSynthesizer speechSynthesizer = new SpeechSynthesizer(context, "TtsMode", selfObject);
            // 此处需要将setApiKey方法的两个参数替换为你在百度开发者中心注册应用所得到的apiKey和secretKey
            speechSynthesizer.setApiKey("gv0ku4i83jjQyGXnM2RuQ6GQ", "MjcrsadVzHlZR7btuKd1F3GiIFDl9euE");
            speechSynthesizer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            selfObject.speechSynthesizer=speechSynthesizer;
        }
        return selfObject;
    }

    public synchronized void dealQueue(){
        if(ifBusy) return;
        if(ttsDataCurrent==null || ttsDataCurrent.ifListenUsed){
            ttsDataCurrent= ttsDataQueue.poll();
        }
        if(ttsDataCurrent==null) return;
        ifBusy=true;

        final String speechText;
        if(ttsDataCurrent.current+384>ttsDataCurrent.content.length()){
            speechText=ttsDataCurrent.content.substring(ttsDataCurrent.current,ttsDataCurrent.content.length());
            ttsDataCurrent.current = ttsDataCurrent.content.length();
        }else{
            int pos=ttsDataCurrent.content.indexOf("。",ttsDataCurrent.current+256);
            pos=Math.min(ttsDataCurrent.current+384,pos);
            speechText= ttsDataCurrent.content.substring(ttsDataCurrent.current,pos+1);
            ttsDataCurrent.current = pos+1;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                int ret=speechSynthesizer.speak(speechText);
                Log.v("TtsMode",speechText);
                if(ret!=0) {
                    String errorDescription = SpeechError.errorDescription(ret);
                    handler.sendMessage(handler.obtainMessage(ret,new SpeechError(ret,errorDescription)));
                }
            }
        }).start();
    }

    public synchronized void pollQueue(int type){
        if(ttsDataCurrent!=null&& !ttsDataCurrent.ifListenUsed){
            ttsDataCurrent.ifListenUsed=true;
            if(ttsDataCurrent.listen!=null){
                if(type == TtsMode.TYPE_SUCCESS){
                    if(ttsDataCurrent.current>=ttsDataCurrent.content.length()){
                        ttsDataCurrent.listen.onFinish(type);
                    }
                }else{
                    ttsDataCurrent.listen.onFinish(type);
                }
            }
        }
        ifBusy=false;
        dealQueue();
    }

    public void speak(String s){
        this.speak(s,null);
    }
    public void speakMonopoly(String s){
        this.cancel();
        this.speak(s,null);
    }
    public void speakMonopoly(String s, TtsListener listen){
        this.cancel();
        this.speak(s, listen);
    }
    public void speak(String s, TtsListener listen){
        ttsDataQueue.offer(new TtsData(s, listen));
        dealQueue();
    }

    public void cancel(){
        while (ttsDataQueue.peek()!=null){
            TtsData t=ttsDataQueue.poll();
            if(t.listen!=null) t.listen.onFinish(TtsMode.TYPE_CANCEL);
        }
        speechSynthesizer.cancel();
    }

    @Override
    public void onStartWorking(SpeechSynthesizer speechSynthesizer) {
    }

    @Override
    public void onSpeechStart(SpeechSynthesizer speechSynthesizer) {
    }

    @Override
    public void onNewDataArrive(SpeechSynthesizer speechSynthesizer, byte[] bytes, int i) {
    }

    @Override
    public void onBufferProgressChanged(SpeechSynthesizer speechSynthesizer, int i) {
    }

    @Override
    public void onSpeechProgressChanged(SpeechSynthesizer speechSynthesizer, int i) {
    }

    @Override
    public void onSpeechPause(SpeechSynthesizer speechSynthesizer) {
    }

    @Override
    public void onSpeechResume(SpeechSynthesizer speechSynthesizer) {
    }

    @Override
    public void onCancel(SpeechSynthesizer speechSynthesizer) {
        pollQueue(TtsMode.TYPE_CANCEL);
    }

    @Override
    public void onSpeechFinish(SpeechSynthesizer speechSynthesizer) {
        pollQueue(TtsMode.TYPE_SUCCESS);
    }

    @Override
    public void onError(SpeechSynthesizer speechSynthesizer, SpeechError speechError) {
        pollQueue(TtsMode.TYPE_ERROR);
        String errorDescription = SpeechError.errorDescription(speechError.errorCode);
        Log.e("TtsMode", errorDescription + "(" + speechError.errorCode + ")");
    }

    protected class TtsData{
        protected String content;
        protected int current=0;
        protected boolean ifListenUsed=false;
        protected TtsListener listen;
        public TtsData(String content, TtsListener listen) {
            this.content = content;
            this.listen = listen;
        }
    }

    public interface TtsListener{
        public void onFinish(int type);
    }

    private void setParams() {
        SpeechLogger.setLogLevel(SpeechLogger.SPEECH_LOG_LEVEL_OFF);
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEAKER, "0");
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_VOLUME, "5");
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_SPEED, "5");
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PITCH, "5");
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_AUDIO_ENCODE, "1");
        speechSynthesizer.setParam(SpeechSynthesizer.PARAM_AUDIO_RATE, "4");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_LANGUAGE, "ZH");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_NUM_PRON, "0");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_ENG_PRON, "0");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_PUNC, "0");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_BACKGROUND, "0");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_STYLE, "0");
        // speechSynthesizer.setParam(SpeechSynthesizer.PARAM_TERRITORY, "0");
    }
}
