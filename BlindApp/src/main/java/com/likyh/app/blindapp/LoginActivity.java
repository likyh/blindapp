package com.likyh.app.blindapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.likyh.app.blindapp.data.BaseData;
import com.likyh.app.blindapp.data.UserMode;
import com.likyh.app.blindapp.view.MyDialogManager;

/**
 * Created by linyh on 14-10-9.
 */
public class LoginActivity extends Activity {
    private UserMode userMode;
    protected MyDialogManager myDialogManager;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO 接收消息并且去更新UI线程上的控件内容
            if(!messageListener(msg)){
                super.handleMessage(msg);
            }
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userMode = UserMode.init(getApplicationContext());
        myDialogManager=new MyDialogManager(LoginActivity.this);
        changeToLogin();
    }

    protected void changeToLogin(){
        setContentView(R.layout.login_layout);
        Button goToRegisterButton=(Button)findViewById(R.id.goToRegister);
        goToRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeToRegister();
            }
        });

        Button loginConfirmButton=(Button)findViewById(R.id.loginConfirmButton);
        loginConfirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                myDialogManager.progressCreate("Loading","加载中，请稍后").show();
                new Thread(new Runnable(){
                    @Override
                    public void run() {
                        final EditText loginUserText=(EditText)findViewById(R.id.loginUserText);
                        final EditText loginPassText=(EditText)findViewById(R.id.loginPassText);
                        BaseData<UserMode.UserData> a = userMode.login(loginUserText.getText().toString(), loginPassText.getText().toString());
                        handler.sendMessage(handler.obtainMessage(0,a));
                    }
                }).start();
            }
        });
    }

    protected void changeToRegister(){
        setContentView(R.layout.register_layout);
        Button goToLoginButton=(Button)findViewById(R.id.goToLogin);
        goToLoginButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                changeToLogin();
            }
        });

        Button loginConfirmButton=(Button)findViewById(R.id.registerConfirmButton);
        loginConfirmButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
            }
        });
    }

    protected boolean messageListener(Message m){
        BaseData<UserMode.UserData> data=(BaseData<UserMode.UserData>)m.obj;

        ((ProgressDialog)myDialogManager.find("Loading")).dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("您的信息");
        if(data!=null){
            if(data.getState()==200&&data.getData()!=null){
                builder.setMessage(data.getData().getUser());
            }else{
                builder.setMessage(data.getMessage());
            }
        }else{
            builder.setMessage("网络请求错误");
        }
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        builder.create().show();
        return true;
    }
}