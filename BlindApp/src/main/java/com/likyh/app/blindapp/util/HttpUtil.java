package com.likyh.app.blindapp.util;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Map;

/**
 * Created by Likyh-linyh on 14-10-15.
 */
public class HttpUtil {
    public static String Get(String host,String path, Map<String, String> params) throws IOException {
        StringBuilder queryString=new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            queryString.append("&");
            queryString.append(URLDecoder.decode(entry.getKey(),"UTF-8"));
            queryString.append("=");
            queryString.append(URLDecoder.decode(entry.getValue(),"UTF-8"));
        }
        if(!path.contains("?")){
            queryString.setCharAt(0,'?');
            return HttpUtil.httpGet(host, path + queryString.toString());
        }else{
            return HttpUtil.httpGet(host,path+queryString.toString());
        }
    }
    public static String httpGet(String host,String path) throws IOException {
        URL url = new URL("http",host,path);
        Log.v("api", url.toString() + "↓");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(3000);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Connection", "Keep-Alive");
        conn.setRequestProperty("Charset", "UTF-8");
        conn.setRequestProperty("Content-type", "multipart/form-data;boundary=*****");
        if (200 == conn.getResponseCode()){
            InputStream httpStream = conn.getInputStream();

            ByteArrayOutputStream outStream = new ByteArrayOutputStream();
            int i;
            while((i = httpStream.read())!=-1){
                outStream.write(i);
            }
            Log.v("api", "["+conn.getResponseCode()+"]"+outStream.toString());
            return outStream.toString();
        }else{
            Log.v("api", "["+conn.getResponseCode()+"]");
            throw new IOException("网络请求错误，"+conn.getResponseCode());
        }
    }



}