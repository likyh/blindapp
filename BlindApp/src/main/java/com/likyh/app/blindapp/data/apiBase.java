package com.likyh.app.blindapp.data;

import android.util.Log;

import com.likyh.app.blindapp.util.HttpUtil;

import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

/**
 * Created by linyh on 2014/10/16.
 */
public class ApiBase {
//    public final String apiHost="169.254.69.213";
//    public final String apiRoot="blindapp";
    public final String apiHost="blindapp.duapp.com";
    public final String apiRoot="";
    public static String token;
    protected String getData(String apiPath,Map<String, String> params){
        String resultString=null;
        try {
            resultString = HttpUtil.Get(apiHost, apiRoot+apiPath, params);
        } catch (IOException e) {
            //TODO 网络错误需要处理
            e.printStackTrace();
        }
        return resultString;
    }

    public String makeMD5(String password) {
        MessageDigest md;
        try {
            // 生成一个MD5加密计算摘要
            md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(password.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            password = new BigInteger(1, md.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return password;
    }

}
