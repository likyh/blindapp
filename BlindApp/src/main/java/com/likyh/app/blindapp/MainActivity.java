package com.likyh.app.blindapp;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TabHost;

/**
 * Created by linyh on 2014/10/21.
 */
public class MainActivity extends TabActivity implements TabHost.TabContentFactory {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TabHost tabHost = getTabHost();

        LayoutInflater.from(this).inflate(R.layout.main_tab, tabHost.getTabContentView(), true);

        tabHost.addTab(tabHost.newTabSpec("新闻").setIndicator("新闻").setContent(this));
        tabHost.addTab(tabHost.newTabSpec("读书").setIndicator("读书").setContent(this));
        tabHost.addTab(tabHost.newTabSpec("精品").setIndicator("精品").setContent(this));
        tabHost.addTab(tabHost.newTabSpec("精品").setIndicator("精品").setContent(new Intent(this,LoginActivity.class)));
    }

    @Override
    public View createTabContent(String tag) {
        View tabView = getLayoutInflater().inflate( R.layout.news, null ) ; //hiddenView是隐藏的View，
        return tabView;
    }
}
