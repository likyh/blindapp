package com.likyh.app.blindapp.data;

/**
 * Created by linyh on 2014/10/16.
 */
public class VoiceMode extends ApiBase {
    private static VoiceMode selfObject;

    public VoiceMode(){
    }
    public static VoiceMode init(){
        if(selfObject==null){
            selfObject=new VoiceMode();
        }
        return selfObject;
    }

    public class VoiceData {
        protected int id;
        protected String title;
        protected String url;
    }
}
