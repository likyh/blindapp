package com.likyh.app.blindapp.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created by linyh on 2014/10/16.
 */
public class NewsMode extends ApiBase {
    protected final String newsApi="/api/news/list";
    private static NewsMode selfObject;
    protected int currentIndex=0;
    protected NewsListData list;

    public NewsMode(){
    }
    public static NewsMode init(){
        if(selfObject==null){
            selfObject=new NewsMode();
        }
        return selfObject;
    }
    public BaseData<NewsListData> list(){ return list(20,0,false,false); }
    public BaseData<NewsListData> list(int rows,int offset){ return list(rows,offset,false,false); }
    public BaseData<NewsListData> list(int rows,int offset,boolean ifArticle){
        return list(rows,offset,ifArticle,false);
    }
    public BaseData<NewsListData> list(int rows,int offset,boolean ifArticle,boolean ifVoice){
        Map<String, String> params=new Hashtable<String, String>();
        params.put("rows",String.valueOf(rows));
        params.put("offset",String.valueOf(offset));
        params.put("ifArticle",ifArticle?"1":"0");
        params.put("ifVoice",ifVoice?"1":"0");
        String jsonString=this.getData(newsApi, params);

        Gson gson = new Gson();
        BaseData<NewsListData> data = null;
        try{
            data = gson.fromJson(jsonString, new TypeToken<BaseData<NewsListData>>() {}.getType());
            list=data.getData();
        }catch (JsonSyntaxException e){
            e.printStackTrace();
        }
        return data;
    }


    public NewsData getCurrent(){
        return list.getAt(currentIndex);
    }

    public int indexChangeCycle(int a){
        currentIndex+=a%list.count;
        if(currentIndex<0){
            currentIndex+=list.count;
        }else if(currentIndex>=list.count){
            currentIndex-=list.count;
        }
        return currentIndex;
    }


    public class NewsListData {
        protected int total;
        protected int count;
        protected List<NewsData> list;

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<NewsData> getList() {
            return list;
        }

        public void setList(List<NewsData> list) {
            this.list = list;
        }

        public NewsData getAt(int a){
            return list.get(a);
        }
    }

    public class NewsData implements Serializable {
        protected int id;
        protected int news_article_id;
        protected String title;
        protected String content;
        protected String date;
        protected String create_time;
        protected int level;
        protected List<VoiceMode.VoiceData> voice;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getNews_article_id() {
            return news_article_id;
        }

        public void setNews_article_id(int news_article_id) {
            this.news_article_id = news_article_id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public List<VoiceMode.VoiceData> getVoice() {
            return voice;
        }

        public void setVoice(List<VoiceMode.VoiceData> voice) {
            this.voice = voice;
        }

        @Override
        public String toString() {
            return "NewsData{" +
                    "id=" + id +
                    ", news_article_id=" + news_article_id +
                    ", title='" + title + '\'' +
                    ", content='" + content + '\'' +
                    ", date='" + date + '\'' +
                    ", create_time='" + create_time + '\'' +
                    ", level=" + level +
                    ", voice=" + voice +
                    '}';
        }
    }
}
