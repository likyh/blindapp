package com.likyh.app.blindapp;

import android.os.Bundle;
import android.util.Log;

import com.likyh.app.blindapp.data.NewsMode;
import com.likyh.app.blindapp.data.TtsMode;

/**
 * Created by linyh on 2014/10/21.
 */
public class NewsDetailGuideActivity extends GuideActivity {
    private NewsMode newsMode;
    private boolean ifStart=false;
    private boolean ifPlay=false;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.v("newsDetail", ttsMode.toString());
        newsMode = NewsMode.init();
        ttsMode.speakMonopoly("当前是新闻朗读页面，您可以逆时针旋转一周回到上一页，长按可开始播放。");
        ttsMode.speak("新闻标题：" + newsMode.getCurrent().getTitle() + " 发布时间：" + newsMode.getCurrent().getDate());
        ttsMode.speak("长按可开始播放。");
        setConfirmText("播放/暂停");
        setLeftText(null);
        setRightText(null);
        setUpText("上一篇新闻");
        setDownText("下一篇新闻");
        setClockText(null);
        setAntiClockText(null);
        setTipsText(newsMode.getCurrent().getTitle());
    }

    protected void newsChangeWith(int a){
        newsMode.indexChangeCycle(a);
        setTipsText(newsMode.getCurrent().getTitle());
        ttsMode.speakMonopoly("新闻标题：" + newsMode.getCurrent().getTitle() + " 发布时间：" + newsMode.getCurrent().getDate());
        ttsMode.speak("长按可开始播放。");
    }

    @Override
    public boolean onDownEvent() {
        newsChangeWith(1);
        return true;
    }

    @Override
    public boolean onUpEvent() {
        newsChangeWith(-1);
        return true;
    }

    @Override
    public boolean onAntiClockEvent() {
        ttsMode.cancel();
        this.finish();
        return true;
    }

    @Override
    public boolean onConfirmEvent() {
        if(!ifStart){
            ttsMode.speakMonopoly(" 新闻主要内容如下：" + newsMode.getCurrent().getContent());
            ifStart=true;
            ifPlay=true;
        }else if(ifPlay){
            ttsMode.speechSynthesizer.pause();
            ifPlay=false;
        }else{
            ttsMode.speechSynthesizer.resume();
            ifPlay=true;
        }
        return true;
    }
}
