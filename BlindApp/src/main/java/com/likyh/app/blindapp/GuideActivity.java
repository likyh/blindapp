package com.likyh.app.blindapp;

import android.app.Activity;
import android.gesture.GestureOverlayView;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.likyh.app.blindapp.data.TtsMode;
import com.likyh.app.blindapp.util.GuideScanner;

/**
 * Created by linyh on 2014/10/21.
 */
abstract public class GuideActivity extends Activity implements GuideScanner.GuideListener {
    private GuideScanner guideScanner;
    protected TtsMode ttsMode;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.guide);

        ttsMode=TtsMode.init(getApplicationContext());

        guideScanner=new GuideScanner(this);
        // 手势画板
        GestureOverlayView gestures = (GestureOverlayView) findViewById(R.id.guideGesture);
        // 普通手势监听器
        gestures.setOnTouchListener(guideScanner);
        // 顺逆时针监听器
        gestures.addOnGesturePerformedListener(guideScanner);

        guideScanner.setListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        ttsMode.speechSynthesizer.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        ttsMode.speechSynthesizer.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        ttsMode.cancel();
    }

    private void updateItem(String s, TextView text,ImageView pic){
        if(s!=null&& !s.isEmpty()){
            text.setText(s);
            text.setVisibility(View.VISIBLE);
            if(pic!=null) pic.setVisibility(View.VISIBLE);
        }else{
            text.setVisibility(View.INVISIBLE);
            if(pic!=null) pic.setVisibility(View.INVISIBLE);
        }
    }

    protected void setLeftText(String s) {
        final TextView guideLeftText=(TextView)findViewById(R.id.guideLeftText);
        final ImageView guideLeftPic=(ImageView)findViewById(R.id.guideLeftPic);
        updateItem(s,guideLeftText, guideLeftPic);
    }

    protected void setRightText(String s) {
        final TextView guideRightText=(TextView)findViewById(R.id.guideRightText);
        final ImageView guideRightPic=(ImageView)findViewById(R.id.guideRightPic);
        updateItem(s,guideRightText, guideRightPic);
    }

    protected void setUpText(String s) {
        final TextView guideUpText=(TextView)findViewById(R.id.guideUpText);
        final ImageView guideUpPic=(ImageView)findViewById(R.id.guideUpPic);
        updateItem(s,guideUpText, guideUpPic);
    }

    protected void setDownText(String s) {
        final TextView guideDownText=(TextView)findViewById(R.id.guideDownText);
        final ImageView guideDownPic=(ImageView)findViewById(R.id.guideDownPic);
        updateItem(s,guideDownText, guideDownPic);
    }

    protected void setClockText(String s) {
        final TextView guideClockText=(TextView)findViewById(R.id.guideClockText);
        updateItem(s,guideClockText, null);
    }

    protected void setAntiClockText(String s) {
        final TextView guideAntiClockText=(TextView)findViewById(R.id.guideAntiClockText);
        updateItem(s,guideAntiClockText, null);
    }

    protected void setConfirmText(String s) {
        final TextView guideConfirmText=(TextView)findViewById(R.id.guideConfirmText);
        final ImageView guideConfirmPic=(ImageView)findViewById(R.id.guideConfirmPic);
        updateItem(s,guideConfirmText, guideConfirmPic);
    }

    protected void setTipsText(String s) {
        final TextView guideTipsText=(TextView)findViewById(R.id.guideTipsText);
        updateItem(s,guideTipsText, null);
    }

    protected void setUnknownText(String s) {
    }

    @Override
    public boolean onLeftEvent() {
        return false;
    }

    @Override
    public boolean onRightEvent() {
        return false;
    }

    @Override
    public boolean onUpEvent() {
        return false;
    }

    @Override
    public boolean onDownEvent() {
        return false;
    }

    @Override
    public boolean onClockEvent() {
        return false;
    }

    @Override
    public boolean onAntiClockEvent() {
        return false;
    }

    @Override
    public boolean onConfirmEvent() {
        return false;
    }

    @Override
    public boolean onUnknownEvent() {
        return false;
    }
}
