package com.likyh.app.blindapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.likyh.app.blindapp.data.TtsMode;

public class WelcomeActivity extends Activity {
    private final int UPDATE_TIMER =0;
    private final int UPDATE_COMPLETE=1;
    private int currentTimer=3;
    protected TtsMode ttsMode;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            // TODO 接收消息并且去更新UI线程上的控件内容
            if(!messageListener(msg)){
                super.handleMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome);
        ttsMode=TtsMode.init(getApplicationContext());

        ttsMode.speak("请问你是否需要语音导航，如果需要，则无需任何操作，三秒后自动打开语音导航。",
                new TtsMode.TtsListener() {
                    @Override
                    public void onFinish(int type) {
                        handler.sendEmptyMessageDelayed(UPDATE_TIMER, 1000);
                    }
                });
        handler.sendEmptyMessageDelayed(UPDATE_COMPLETE, 20000);

        PreferenceManager.getDefaultSharedPreferences(this).edit().putBoolean("firstUse", false).apply();
    }

    protected boolean messageListener(Message m){
        switch (m.what){
            case UPDATE_TIMER:
                if(currentTimer>1){
                    currentTimer--;
                    TextView textView=(TextView)findViewById(R.id.timeLeft);
                    textView.setText(String.valueOf(currentTimer));
                    handler.sendEmptyMessageDelayed(UPDATE_TIMER, 1000);
                }else{
                    handler.sendEmptyMessageDelayed(UPDATE_COMPLETE, 1000);
                }
                break;
            case UPDATE_COMPLETE:
                handler.removeMessages(UPDATE_COMPLETE);
                new Handler().post(new Runnable() {
                    public void run() {
                        Intent mainIntent = new Intent(WelcomeActivity.this,NewsGuideActivity.class);
                        WelcomeActivity.this.startActivity(mainIntent);
                        WelcomeActivity.this.finish();
                    }
                });
        }
        return true;
    }
}
